package sycho.example.springboot.post.service;

import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sycho.example.springboot.member.domain.Member;
import sycho.example.springboot.post.domain.Post;
import sycho.example.springboot.post.dto.PostRequest;
import sycho.example.springboot.post.dto.PostResponse;
import sycho.example.springboot.post.repository.PostRepository;

@Service
@RequiredArgsConstructor
public class PostService {
    private final PostRepository postRepository;

    @Transactional
    public Post save(PostRequest postRequest) {
        return postRepository.save(postRequest.toDomain());
    }

    public List<PostResponse> findAll(Member member, String nickname) {
        List<Post> posts = postRepository.findAll();
        return posts.stream()
            .map(PostResponse::new)
            .collect(Collectors.toList());
    }

    public PostResponse find(Member member, Integer id) {
        return postRepository.findById(id)
            .map(PostResponse::new)
            .orElseThrow();
    }

    @Transactional
    public String modify(Integer id, PostRequest postRequest) {
        String message = "fail";
        Post post = postRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("해당 게시글이 없습니다."));

        // 작성자만 수정 가능
        if (post.getMember().getId().equals(postRequest.getMember().getId())) {
            post.update(postRequest.getContents());
            message = "success";
        }
        return message;
    }

    @Transactional
    public String remove(Integer id, Member member) {
        String message = "fail";
        Post post = postRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("해당 게시글이 없습니다."));

        if (post.getMember().getId().equals(member.getId())) { // 작성자만 삭제 가능
            postRepository.delete(post);
            message = "success";
        }

        return message;

    }
}
