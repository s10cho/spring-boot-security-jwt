package sycho.example.springboot.post.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sycho.example.springboot.post.domain.Post;

public interface PostRepository extends JpaRepository<Post, Integer> {
}
