package sycho.example.springboot.post.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sycho.example.springboot.post.dto.PostRequest;
import sycho.example.springboot.post.dto.PostResponse;
import sycho.example.springboot.post.service.PostService;
import sycho.example.springboot.share.security.SecurityUser;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/post")
public class PostController {

    private final PostService postService;

    @PostMapping
    public String register(
        @RequestBody PostRequest postRequest,
        @AuthenticationPrincipal SecurityUser principal
    ) {
        postRequest.setMember(principal.getMember());
        postService.save(postRequest);
        return "success";
    }

    @GetMapping()
    public List<PostResponse> findAll(
        @AuthenticationPrincipal SecurityUser principal,
        @RequestParam(value = "nickname", required = false) String nickname
    ) {
        return postService.findAll(principal.getMember(), nickname);
    }

    @GetMapping("/{id}")
    public PostResponse find(
        @AuthenticationPrincipal SecurityUser principal,
        @PathVariable Integer id
    ) {
        return postService.find(principal.getMember(), id);
    }

    @PutMapping("{id}")
    public String modify(
        @AuthenticationPrincipal SecurityUser principal,
        @PathVariable Integer id,
        @RequestBody PostRequest postRequest
    ) {
        postRequest.setMember(principal.getMember());
        return postService.modify(id, postRequest);
    }

    @DeleteMapping("{id}")
    public String delete(
        @AuthenticationPrincipal SecurityUser principal,
        @PathVariable Integer id
    ) {
        return postService.remove(id, principal.getMember());
    }
}
