package sycho.example.springboot.post.dto;

import static sycho.example.springboot.share.util.DisplayTimeUtil.format;

import lombok.Getter;
import sycho.example.springboot.post.domain.Post;

@Getter
public class PostResponse {

    private Integer id;

    private String content;

    private String nickname;

    private String profilePath;

    private String createdAt;

    public PostResponse(Post post) {
        this.id = post.getId();
        this.nickname = post.getMember().getNickname();
        this.profilePath = post.getMember().getProfilePath();
        this.content = post.getContent();
        this.createdAt = format(post.getCreatedAt());
    }
}
