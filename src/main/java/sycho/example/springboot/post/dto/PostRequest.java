package sycho.example.springboot.post.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import sycho.example.springboot.member.domain.Member;
import sycho.example.springboot.post.domain.Post;

@Getter
@NoArgsConstructor
public class PostRequest {

    private String contents;

    private Member member;

    @Builder
    public PostRequest(String contents, Member member) {
        this.contents = contents;
        this.member = member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Post toDomain() {
        return Post.builder()
            .content(contents)
            .member(member)
            .build();
    }
}