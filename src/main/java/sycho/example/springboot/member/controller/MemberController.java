package sycho.example.springboot.member.controller;

import java.util.Map;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sycho.example.springboot.member.service.MemberService;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class MemberController {

    private final MemberService memberService;

    @PostMapping("/member")
    public UUID register(@RequestBody Map<String, String> user) {
        return memberService.register(user);
    }

    @PostMapping("/login")
    public String login(@RequestBody Map<String, String> user) {
        return memberService.login(user);
    }
}
