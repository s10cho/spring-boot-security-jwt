package sycho.example.springboot.member.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import sycho.example.springboot.member.domain.Member;

public interface MemberRepository extends JpaRepository<Member, String> {
    Optional<Member> findByEmail(String email);
}
