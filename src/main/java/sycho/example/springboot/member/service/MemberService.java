package sycho.example.springboot.member.service;

import java.util.Map;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sycho.example.springboot.config.JwtTokenProvider;
import sycho.example.springboot.member.domain.Member;
import sycho.example.springboot.member.domain.Role;
import sycho.example.springboot.member.repository.MemberRepository;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final PasswordEncoder passwordEncoder;

    private final JwtTokenProvider jwtTokenProvider;

    private final MemberRepository memberRepository;

    public UUID register(Map<String, String> user) {
        Role role = Role.of(user.getOrDefault("role", "guest"));
        return memberRepository.save(
            Member.builder()
                .email(user.get("email"))
                .password(passwordEncoder.encode(user.get("password")))
                .nickname(user.get("nickname"))
                .phone(user.get("phone"))
                .role(role)
                .build()).getId();
    }

    public String login(Map<String, String> user) {
        Member member = memberRepository.findByEmail(user.get("email"))
            .orElseThrow(() -> new IllegalArgumentException("가입 되지 않은 이메일입니다."));

        if (!passwordEncoder.matches(user.get("password"), member.getPassword())) {
            throw new IllegalArgumentException("이메일 또는 비밀번호가 맞지 않습니다.");
        }

        return jwtTokenProvider.createToken(member.getEmail(), member.getRole());
    }
}
