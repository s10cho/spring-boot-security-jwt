package sycho.example.springboot.member.service;

import java.util.Optional;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sycho.example.springboot.member.domain.Member;
import sycho.example.springboot.member.repository.MemberRepository;
import sycho.example.springboot.share.security.SecurityUser;

@Service
@RequiredArgsConstructor
public class SecurityUserDetailService implements UserDetailsService {

    private final MemberRepository memberRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Member> findMember = memberRepository.findByEmail(username);
        if (findMember.isEmpty()) {
            throw new UsernameNotFoundException(username + " 사용자 없음");
        }

        Member member = findMember.get();
        return new SecurityUser(member);
    }
}
