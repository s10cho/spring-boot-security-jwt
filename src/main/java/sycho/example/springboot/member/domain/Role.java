package sycho.example.springboot.member.domain;

public enum Role {
    GUEST,
    MEMBER,
    ADMIN;

    public static Role of(String role) {
        return switch (role) {
            case "member" -> MEMBER;
            case "admin" -> ADMIN;
            default -> GUEST;
        };
    }
}
