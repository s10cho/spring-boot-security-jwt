package sycho.example.springboot.share.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DisplayTimeUtil {

    public static final String YYYY_MM_DD = "yyyy.MM.dd";

    public static final SimpleDateFormat FORMAT = new SimpleDateFormat(YYYY_MM_DD);

    public static String format(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern(YYYY_MM_DD));
    }

    public static String format(long time) {
        return FORMAT.format(time);
    }
}
