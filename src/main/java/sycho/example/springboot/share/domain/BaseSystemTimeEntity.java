package sycho.example.springboot.share.domain;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseSystemTimeEntity {

    @CreatedDate
    @Column(updatable = false)
    private long createdAt;

    @LastModifiedDate
    private long updatedAt;

    @PrePersist
    public void onPrePersist() {
        long currentTimeMillis = System.currentTimeMillis();
        this.createdAt = currentTimeMillis;
        this.updatedAt = currentTimeMillis;
    }

    @PreUpdate
    public void onPreUpdate() {
        this.updatedAt = System.currentTimeMillis();
    }
}